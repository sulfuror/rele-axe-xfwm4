# Rele Axe xfwm4

This is a fork of [Rele](https://www.xfce-look.org/content/show.php/Rele?content=77260)
and [Axe](https://www.xfce-look.org/content/show.php/axe?content=73291) themes.
Love them both so I decided to make a fork of Rele with the light look and
red buttons (when pressed) of Axe theme.

Works fine but buttons are tricky; so it is windows resize with the mouse.

![Rele/Axe light](https://gitlab.com/sulfuror/rele-axe-xfwm4/raw/master/Rele-Axe/Screenshot_2018-10-24_12-14-44.png)

![Rele/Axe dark](https://gitlab.com/sulfuror/rele-axe-xfwm4/raw/master/Rele-Axe/Screenshot_2018-10-24_12-37-51.png)